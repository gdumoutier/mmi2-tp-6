<?php include_once('_inc/config.php')  ?>

<!doctype html>
<html class="no-js" lang="">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title><?php echo $title; ?></title>
	<meta name="description" content="<?php echo $description; ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
</head>
<body class="saison-<?php echo $saison ?>">

	<header role="banner">
		<nav>
			<ul>
				<li><a href="<?php echo $route['home']; ?><?php saison(true); ?>">Accueil</a></li>
				<li><a href="<?php echo $route['about']; ?><?php saison(true); ?>">Histoire et patrimoine</a></li>
				<li><a href="<?php echo $route['activities-index']; ?><?php saison(true); ?>">Activit�s</a></li>
				<li><a href="<?php echo $route['contact']; ?><?php saison(true); ?>">Nous contacter</a></li>
			</ul>
		</nav>
	</header>

	<main>
	
		<!--  
		<p>
			changer de saison : 
			<ul>
				<li><a href="<?= currentUrlSaison(1); ?>">Printemp</a></li>
				<li><a href="<?= currentUrlSaison(2); ?>">�t�</a></li>
				<li><a href="<?= currentUrlSaison(3); ?>">Automn</a></li>
				<li><a href="<?= currentUrlSaison(4); ?>">Hiver</a></li>
			</ul>
		</p>
		-->


