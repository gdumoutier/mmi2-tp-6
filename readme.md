#TP : Office de Tourisme

À l'aide de BootStrap 3 (LESS), réalisez la page d'accueil de l'office de Tourisme de la ville réelle ou imaginaire de votre choix. Voici le cahier des charges :

- Mettez en application vos acquis en HTML5 sémantique
- Utilisez du "Lorem Ipsum" pour les textes
- Votre design doit avoir une couleur dominante qui changera selon la saison : 
	- printemps : vert
	- été : jaune
	- automne : orange
	- hiver : bleu 
- Chaque saison applique une classe sur l'élément <body> : .saison-1, saison-2, etc.
- Vous pouvez changer la saison grâce à un paramètre GET, exemple : **page.php?saison=3**.
- Utilisez les caractéristiques du préprocesseur LESS pour cela.
- Utilisez Bootstrap 3 LESS et appliquez autant que possible les patterns offerts par cette bibliothèque.
- Utilisez jQuery et des plugins si besoin.
- Pour créer vos tuiles et leurs variantes, créez une mixin .make-card()
- Le site doit être responsive
- Dans votre fichier de style, vos définirez toutes vos variables après les @import.
- Votre fichier de style ne fera jamais appel aux couleurs directement, réutilisez vos variables.
- Profitez des fonctions natives de LESS tel que fade(), saturate(), lighten(), mix(), etc.
- Le graphisme doit être simple.

## Header

- Placez le logo (qui fera office de liens vers la page d'accueil) et la navigation dans le fichier _inc/header.php
- La navigation comprendra les liens suivants : 
	- Accueil  (index.php)
	- Histoire & patrimoine  (about.php)
	- Activités (activities-index.php)
	- Contact (contact.php)
- Le **Header** comprendra aussi la présence de liens sociaux : **Facebook**, **Youtube** et **Instagram**. 
- Élaborez une mixin **.make-social-button(@name,@color)** pour changer la couleur de ces derniers.
- Ajoutez une couche d'affordance et appliquez une emphase sur le lien courant de la navigation

## Les pages

- La page **index** comprendra les sections suivantes en plus du **Header** et du **Footer** :
	- Un slider / hero (image prenant toute la largeur de l'écran) de 3 visuels.
	- Un édito.
	- 4 activités sous forme de tuile.
	- Une carte interactive indiquant l'emplacement de l'office de tourisme.
- La page **about** est une simple page de contenu (article). Vous effectuerez une mise en page aboutie de 1600 mots environ avec des images.
- La page **activities-index** comprendra une pagination (3 pages). Utilisez PHP pour mettre en emphase la page courante. Chaque page comportera 9 tuiles d'activités. Chaque tuile changera d'apparence en fonction de la saison.
- La page **contact** doit afficher les champs suivants : nom, prénom, mail, objet, textarea. Utilisez les patterns offerts par BootStrap 3.
- La page **activities-show** comprendra un slider, un texte de présentation court, une Google map interactive et un bouton de partage Facebook. Vous ne présenterez qu'une seule activité, quelque soit la tuile cliquée à l'origine.

## Footer
- Affichez des coordonnées fictives.
- Il doit comporter une navigation reprenant celle du **Header**.
- Le logo qui pointera vers l'accueil.
