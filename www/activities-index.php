<?php
$current = "activities-index";
if(isset($_GET['page'])){
	$page = $_GET['page'];
}else{
	$page = 1;
}
?>
<?php include_once('_inc/header.php')  ?>

<?php include_once('activities/activities-'.$page.'.php')  ?>

<ul class="paginate">
	<li><a href="<?php echo $route['activities-index']; ?>?page=1<?php saison(); ?>">1</a></li>
	<li><a href="<?php echo $route['activities-index']; ?>?page=2<?php saison(); ?>">2</a></li>
	<li><a href="<?php echo $route['activities-index']; ?>?page=3<?php saison(); ?>">3</a></li>
</ul>

<?php include_once('_inc/footer.php')  ?>
